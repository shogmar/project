SHELL := /bin/bash

tests:
    sudo symfony console doctrine:fixtures:load -n
    sudo symfony php bin/phpunit
.PHONY: tests